#ifndef SENDARP_H
#define SENDARP_H

#include <iostream>
#include <cstdio>
#include <unistd.h>
#include <pcap.h>
#include <string>
#include <cstring>
#include <regex>
#include <fstream>
#include <streambuf>
#include <stdint.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <time.h>
#include "ethhdr.h"
#include "arphdr.h"

#define MAC_ADDR_LEN 6
#define IP_ADDR_LEN 4

#pragma pack(push,1)

struct EthArpPacket final{
    EthHdr eth_;
    ArpHdr arp_;
};

#pragma pack(pop)

uint32_t ip_str_to_uint(char* ip);
bool check_arp_reply(const u_char* packet);
uint32_t ip_src_parse(const u_char* packet);
uint32_t ip_dst_parse(const u_char* packet);
bool check_if_src_ip(const u_char* packet, uint32_t ip_);
bool check_if_dst_ip(const u_char* packet, uint32_t ip_);
void mac_src_parse(const u_char* packet, uint8_t* mac_addr);
void arp_make(EthArpPacket* p, uint8_t* dmac, uint8_t* smac, uint16_t op, uint32_t sip, uint32_t dip);
//void deceive_smac_unknown(pcap_t* handle, uint8_t* my_mac, uint32_t* ip_arr, uint8_t* sender_mac);
void deceive_smac_known(pcap_t* handle, uint8_t* my_mac, uint32_t* ip_arr, uint8_t* sender_mac);

#endif // SENDARP_H
