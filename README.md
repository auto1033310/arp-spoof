# Arp Spoof
Custom Arp Spoof program prototype applied Parallel Programming method

## Env
```
apt-get install flex, bison
apt-get install pcap
```

## Compile
if you don't use QT, ignore all \_qt\_* files
```
make
```

## Usage

```
syntax : send-arp <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]
sample : send-arp wlan0 192.168.10.2 192.168.10.1
```