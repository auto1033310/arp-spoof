CFLAGS=-g
LDLIBS=-lpcap
PTHR=-pthread
LPTHR=-lpthread
WL= -Wl,--no-as-needed

all: arp-spoof

main.o: main.cpp send-arp.h

arphdr.o: mac.h ip.h arphdr.h arphdr.cpp

ethhdr.o: mac.h ethhdr.h ethhdr.cpp

ip.o: ip.h ip.cpp

mac.o : mac.h mac.cpp

send-arp.o: send-arp.cpp send-arp.h mac.h ip.h ethhdr.h arphdr.h

arp-spoof: main.o send-arp.o arphdr.o ethhdr.o ip.o mac.o
	$(LINK.cc) $^ $(PTHR) $(LPTHR) $(LOADLIBES) $(LDLIBS) $(WL) -o $@

clean:
	rm -f *.o arp-spoof
